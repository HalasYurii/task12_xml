package task12.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task12.chekers.ExtensionChecker;
import task12.model.bank.Bank;
import task12.parser.dom.ParserUserDom;
import task12.parser.sax.SAXParserUser;
import task12.parser.stax.StAXReader;

import java.io.File;
import java.util.List;

import static task12.model.consts.ConstWays.WAY_BANK_XML;
import static task12.model.consts.ConstWays.WAY_BANK_XSD;

public class Parser {
    private static final Logger LOG = LogManager.getLogger(Parser.class);

    public static void main(String[] args) {
        File xml = new File(WAY_BANK_XML);
        File xsd = new File(WAY_BANK_XSD);

        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            showList(SAXParserUser.parseBanks(xml, xsd), "SAX");

            showList(StAXReader.parseBanks(xml), "StAX");

            showList(ParserUserDom.getBankList(xml), "DOM");
        }
    }

    private static boolean checkIfXSD(File xsd) {
        return ExtensionChecker.isXSD(xsd);
    }

    private static boolean checkIfXML(File xml) {
        return ExtensionChecker.isXML(xml);
    }

    private static void showList(List<Bank> banks, String parserName) {
        LOG.debug("parser: "+parserName);
        for(Bank bank:banks){
            LOG.info(bank);
        }
    }

}
