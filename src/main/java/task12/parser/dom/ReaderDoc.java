package task12.parser.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import task12.model.bank.Bank;
import task12.model.bank.helpers.*;

import java.util.ArrayList;
import java.util.List;

public class ReaderDoc {
    public List<Bank> readDoc(Document doc){
        doc.getDocumentElement().normalize();
        List<Bank> banks = new ArrayList<>();
        NodeList nodeList = doc.getElementsByTagName("bank");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Bank bank = new Bank();

            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;
                bank.setBankNum(Integer.parseInt(element.getAttribute("bankNum")));
                bank.setName(new MyName(element.getElementsByTagName("name").item(0).getTextContent()));
                bank.setCountry(new MyCountry(element.getElementsByTagName("country").item(0).getTextContent()));
                bank.setType(new MyType(element.getElementsByTagName("type").item(0).getTextContent()));
                bank.setMyDepositor(new MyDepositor(element.getElementsByTagName("depositor").item(0).getTextContent()));
                bank.setAccountID(new MyAccountID(element.getElementsByTagName("accountID").item(0).getTextContent()));
                bank.setProfitability(new MyProfitability(Double.parseDouble(element.getElementsByTagName("profitability").item(0).getTextContent())));
                bank.setAmountOnDeposit(new MyAmountOnDeposit(Integer.parseInt(element.getElementsByTagName("amountOnDeposit").item(0).getTextContent())));
                bank.setConstraints(new MyConstraints());
                bank.setConstraints(getConstraints(element.getElementsByTagName("constraints")));
                banks.add(bank);
            }
        }
        return banks;
    }
    private MyConstraints getConstraints(NodeList nodes){
        MyConstraints constraints = new MyConstraints();
        if(nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element)nodes.item(0);
            constraints.setAmount(Double.parseDouble(element.getElementsByTagName("timec").item(0).getTextContent()));
            constraints.setType(element.getElementsByTagName("typec").item(0).getTextContent());
        }
        return constraints;
    }
}
