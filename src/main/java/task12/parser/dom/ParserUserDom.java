package task12.parser.dom;

import org.w3c.dom.Document;
import task12.model.bank.Bank;

import java.io.File;
import java.util.List;

public class ParserUserDom {
    public static List<Bank> getBankList(File xml){
        CreatorDoc creator = new CreatorDoc(xml);
        Document doc = creator.getDocument();
        ReaderDoc reader = new ReaderDoc();
        return reader.readDoc(doc);
    }
}
