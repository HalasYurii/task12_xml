package task12.parser.stax;

import task12.model.bank.Bank;
import task12.model.bank.helpers.*;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StAXReader {
    public static List<Bank> parseBanks(File xml){
        List<Bank> beerList = new ArrayList<>();
        Bank bank = null;
        MyConstraints constraints = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "bank":
                            bank = new Bank();

                            Attribute idAttr = startElement.getAttributeByName(new QName("bankNum"));
                            if (idAttr != null) {
                                bank.setBankNum(Integer.parseInt(idAttr.getValue()));
                            }
                            break;
                        case "name":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(bank).setName(new MyName(xmlEvent.asCharacters().getData()));
                            break;
                        case "country":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(bank).setCountry(new MyCountry(xmlEvent.asCharacters().getData()));
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(bank).setType(new MyType(xmlEvent.asCharacters().getData()));
                            break;
                        case "depositor":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(bank).setMyDepositor(new MyDepositor(xmlEvent.asCharacters().getData()));
                            break;
                        case "accountID":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(bank).setAccountID(new MyAccountID(xmlEvent.asCharacters().getData()));
                            break;
                        case "profitability":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(bank).setProfitability(new MyProfitability(Double.parseDouble(xmlEvent.asCharacters().getData())));
                            break;
                        case "amountOnDeposit":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(bank).setAmountOnDeposit(new MyAmountOnDeposit(Integer.parseInt(xmlEvent.asCharacters().getData())));
                            break;
                        case "constraints":
                            xmlEvent = xmlEventReader.nextEvent();
                            constraints = new MyConstraints();
                            break;
                        case "timec":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(constraints).setAmount(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "typec":
                            xmlEvent = xmlEventReader.nextEvent();
                            Objects.requireNonNull(constraints).setType(xmlEvent.asCharacters().getData());
                            Objects.requireNonNull(bank).setConstraints(constraints);
                            break;
                    }
                }
                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();
                    if(endElement.getName().getLocalPart().equals("bank")){
                        beerList.add(bank);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return beerList;
    }
}
