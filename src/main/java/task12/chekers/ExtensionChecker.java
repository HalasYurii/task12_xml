package task12.chekers;

import org.apache.commons.io.FilenameUtils;
import java.io.File;

import static task12.model.consts.ConstObject.END_XML;
import static task12.model.consts.ConstObject.END_XSD;

public class ExtensionChecker {
    public static boolean isXML(File xml) {
        return xml.isFile() && FilenameUtils.getExtension(xml.getName()).equals(END_XML);
    }

    public static boolean isXSD(File xsd){
        return xsd.isFile() && FilenameUtils.getExtension(xsd.getName()).equals(END_XSD);
    }
}
