package task12.model.consts;

public class ConstObject {
    public static final String END_XML = "xml";
    public static final String END_XSD = "xsd";
    public static final String EMPTY_STIRNG = "";

    private ConstObject() {
    }
}
