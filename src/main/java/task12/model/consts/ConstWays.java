package task12.model.consts;

public class ConstWays {
    public static final String WAY_BANK_XML = "src\\main\\resources\\xml\\banksXML.xml";
    public static final String WAY_BANK_XSD = "src\\main\\resources\\xml\\banksXSD.xsd";


    private ConstWays() {
    }
}
